package demo

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object UpdateStateByKeyDemo {

  def updateFunction(newValues: Seq[Int],  historyValue: Option[Int]): Option[Int] = {
    val newCount =historyValue.getOrElse(0)+newValues.sum
    Option.apply(newCount)
  }

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)

    val conf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc = new StreamingContext(conf, Seconds(2))

    val lineRDD = ssc.socketTextStream("hadoop111", 8080)

    val words = lineRDD.flatMap(_.split(" "))

    ssc.checkpoint("./checkPoint")

    val paris = words.map(word => (word, 1))

    val count = paris.updateStateByKey(updateFunction)
    count.print()

    ssc.start()
    ssc.awaitTermination()
  }

}
