package demo

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object TransformDemo {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val conf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc = new StreamingContext(conf, Seconds(2))

    val lineRDD = ssc.socketTextStream("hadoop111", 8080)

    /**
      * 黑名单 RDD
      */
    val blackRDD = ssc.sparkContext.parallelize(Array(("zs", true), ("lisi", true)))

    /**
      * zs sb sb sb
      * lisi sb sb sb
      * wangwu hello hello hello
      */
    lineRDD.map(x => {
      val info = x.split(" ")

      (info(0), info.toList.tail.mkString(" "))
    }).transform(rdd => {
      /**
        * (zs,(sb sb sb))
        * (lisi, (sb sb sb))
        */
      val joinRDD = rdd.leftOuterJoin(blackRDD)

      /**
        *  (zs, (sb sb sb),Some(true))
        *  (wangwu ,(he he he),None)
        */
      val filterRDD = joinRDD.filter(x => x._2._2.isEmpty)
      filterRDD
    }).map(a => (a._1,a._2._2)).print()

    ssc.start()
    ssc.awaitTermination()
  }
}
