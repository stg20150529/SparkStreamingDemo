package demo


import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * sql 和 streaming 连用
  */
object SQLAndStreaming {

  def updateState(newValues: Seq[Int], historyValues: Option[Int]): Option[Int] = {
    val count = historyValues.getOrElse(0) + newValues.sum
    Option.apply(count)
  }

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)
    val conf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc = new StreamingContext(conf, Seconds(2))

    val lineRDD = ssc.socketTextStream("hadoop111", 8080)

    ssc.checkpoint("./checkPoint")

    val word: DStream[(String, Int)] = lineRDD.flatMap(_.split(" ")).map(word => (word, 1))



    word.updateStateByKey(updateState).foreachRDD(rdd => {
      val session: SparkSession = SparkSession.builder().getOrCreate()
      val rowRDD: RDD[Row] = rdd.map(x => Row(x._1, x._2))
      val schema = StructType(List(
        StructField("word", StringType, true),
        StructField("count", IntegerType, true)
      ))
      // 创建临时视图
      session.createDataFrame(rowRDD,schema).createOrReplaceTempView("wc")
      session.sql("select * from wc where count > 3").show()
    })




    ssc.start()
    ssc.awaitTermination()
  }

}
