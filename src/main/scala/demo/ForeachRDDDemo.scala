package demo

import java.sql.Connection

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import util.JDBCUtil

object ForeachRDDDemo {

  def updateState(newValues: Seq[Int], historyValues: Option[Int]): Option[Int] = {
    val count = historyValues.getOrElse(0) + newValues.sum
    Option.apply(count)
  }

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val conf: SparkConf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc = new StreamingContext(conf, Seconds(2))

    ssc.checkpoint("./checkPoint")

    val lineRDD: ReceiverInputDStream[String] = ssc.socketTextStream("hadoop111", 8080)

    val wordCount: DStream[(String, Int)] = lineRDD.flatMap(_.split(" ")).map(word => (word, 1)).updateStateByKey(updateState)

    wordCount.foreachRDD(rdd => {
      if (!rdd.isEmpty()) {
        rdd.foreachPartition(part => {
          val connection: Connection = JDBCUtil.getConnection()
          part.foreach(data => {
            val sql = s"insert into wordcount(word,count) values('${data._1}','${data._2}') on duplicate key update count = ${data._2}"
            connection.prepareStatement(sql).executeUpdate()
          })
          JDBCUtil.returnConnction(connection)
        })
      }
    })

    ssc.start()
    ssc.awaitTermination()

  }

}
