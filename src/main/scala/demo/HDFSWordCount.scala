package demo

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object HDFSWordCount {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    System.setProperty("hadoop.home.dir", "D:\\hadoop-2.6.0")

    System.setProperty("HADOOP_USER_NAME", "hadoop")

    val conf = new SparkConf().setAppName(this.getClass().getSimpleName()).setMaster("local[2]")

    val ssc = new StreamingContext(conf,Seconds(2))
    println("开始读取")
    val wordCount = ssc.textFileStream("hdfs://hadoop111:9000/input/")
    val sum = wordCount.flatMap(_.split(" ")).map(a=>(a,1)).reduceByKey(_+_)
    sum.print()

    ssc.start()
    ssc.awaitTermination()

  }

}
