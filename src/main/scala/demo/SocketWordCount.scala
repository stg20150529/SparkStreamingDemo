package demo

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  *  接收 Socket 数据
  *  统计个数
  */
object SocketWordCount {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("").setLevel(Level.WARN)

    val conf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    /**
      *  创建 StreamingContext
      *  1：conf
      *  2：时间间隔
      */
    val ssc = new StreamingContext(conf,Seconds(2))
    /**
      *  读取 socket 信息
      */
    val inputDStream = ssc.socketTextStream("hadoop111",8080)
    val words = inputDStream.flatMap(a => a.split(" "))
    val word = words.map(a => (a,1))
    val count = word.reduceByKey((a,b) => a+ b)
    count.print()
    ssc.start() // 开始
    ssc.awaitTermination() // 等待结束
  }

}
