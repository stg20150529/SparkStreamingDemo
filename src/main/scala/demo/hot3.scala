package demo

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  *  取出 热度前三
  */
object hot3 {

  def main(args: Array[String]): Unit = {

    def main(args: Array[String]): Unit = {
      val conf: SparkConf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

      val ssc = new StreamingContext(conf,Seconds(2))

      val lineRDD: ReceiverInputDStream[String] = ssc.socketTextStream("hadoop111",8080)

      val word: DStream[(String, Int)] = lineRDD.flatMap(_.split(" ")).map(word => (word,1))

      word.reduceByKeyAndWindow(
        // x 代表 聚合的结果
        // y 代表 这个 key 下次要聚合的结果
        (x:Int,y:Int) => x + y,
        // 保留多久的数据
        Seconds(10),
        // 多久计算一次
        Seconds(4)
      )
    }.transform(rdd => {
      // 排序 取 前三
      val sort: RDD[(String, Int)] = rdd.sortBy(t => t._2,false)
      val array: Array[(String, Int)] = sort.take(3)
      array.foreach(println)
      sort
    })

  }

}
