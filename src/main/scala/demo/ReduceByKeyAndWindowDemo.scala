package demo

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Ctrl + Alt + V 自动生成变量名
  */
object ReduceByKeyAndWindowDemo {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val conf: SparkConf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc: StreamingContext = new StreamingContext(conf, Seconds(2))

    val lines: ReceiverInputDStream[String] = ssc.socketTextStream("hadoop111", 8080)

    val words: DStream[String] = lines.flatMap(_.split(" "))

    val word: DStream[(String, Int)] = words.map(word => (word, 1))

    /**
      * reduceFunc 函数
      * windowDuration:窗口的宽度,如本例5秒切分一次RDD，框10秒，就会保留最近2次切分的RDD
      * slideDuration:表示window滑动的时间长度，即每隔多久执行本计算
      */
    val value: DStream[(String, Int)] = word.reduceByKeyAndWindow(
       // a 代表聚合后的结果，b 代表这个 Key 对应的下一次需要聚合的值
      (a: Int, b: Int) => a + b,
      // 窗口时间大小
      Seconds(10),
      // 窗口滑动时间
      Seconds(4))
    value.print()

    ssc.start()
    ssc.awaitTermination()
  }

}
