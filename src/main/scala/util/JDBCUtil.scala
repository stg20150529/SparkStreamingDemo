package util

import java.sql.{Connection, DriverManager, SQLException}
import java.util

object JDBCUtil {

  var connectionQue : java.util.LinkedList[Connection] = null

  Class.forName("com.mysql.jdbc.Driver")

  /**
    *  生产 数据库 连接
    * @return
    */
  def getConnection() : Connection = {
    synchronized({
      try{
        if(null == connectionQue){
          connectionQue = new util.LinkedList[Connection]()
          for(i <- 0 until(10)){
            val connection = DriverManager.getConnection("jdbc:mysql://hadoop111:3306/spark","root","root")
            connectionQue.push(connection)
          }
        }
      }catch{
        case e : SQLException => e.printStackTrace()
      }
    })
    connectionQue.poll()
  }

  /**
    *  当连接对象用完后，需要调用此方法归还
    * @param connection
    */
  def returnConnction(connection: Connection): Unit ={
    connectionQue.push(connection)
  }

  def main(args: Array[String]): Unit = {
    val connection: Connection = getConnection()
    returnConnction(connection)
    println(connectionQue.size())
  }

}
