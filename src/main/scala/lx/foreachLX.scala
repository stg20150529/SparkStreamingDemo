package lx

import java.sql.Connection

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import util.JDBCUtil

object foreachLX {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    val conf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc = new StreamingContext(conf,Seconds(2))

    val value: DStream[(String, Int)] = ssc.socketTextStream("hadoop111",8080).flatMap(_.split(" ")).map(word => (word,1))

    value.foreachRDD(rdd => {
      if(!rdd.isEmpty()){
        rdd.foreachPartition(part => {
          val connection: Connection = JDBCUtil.getConnection()
          part.foreach(data => {
            val sql = s"insert into wordcount(word,count) values('${data._1}','${data._2}') on duplicate key update count = ${data._2}"
            connection.prepareStatement(sql).execute()
          })
          JDBCUtil.returnConnction(connection)
        })
      }
    })

    ssc.start()
    ssc.awaitTermination()
  }

}
