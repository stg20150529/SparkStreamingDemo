package lx

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object windowLx {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val conf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc = new StreamingContext(conf,Seconds(2))

    val lineRDD = ssc.socketTextStream("hadoop111",8080)

    lineRDD.flatMap(_.split(" ")).map(word => (word,1)).reduceByKeyAndWindow(
      (x : Int,y:Int) => x+y,
      Seconds(10),
      Seconds(4)
    ).print()

    ssc.start()
    ssc.awaitTermination()
  }

}
