package lx

import java.sql.Connection

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import util.JDBCUtil

object foreachLX2 {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName(this.getClass.getSimpleName).setMaster("local[2]")

    val ssc = new StreamingContext(conf,Seconds(2))

    val lineRDD = ssc.socketTextStream("hadoop111",8080)

    val word: DStream[(String, Int)] = lineRDD.flatMap(_.split(" ").map(word => (word,1)))

    word.foreachRDD(rdd => {
      if(!rdd.isEmpty()){
        rdd.foreachPartition(part=> {
          val connection: Connection = JDBCUtil.getConnection()
          part.foreach(data => {
            val sql = s"insert into wordcount (word,count) values('${data._1}','${data._2}') on duplicate key update count = ${data._2}"
            connection.prepareStatement(sql).execute()
          })
          JDBCUtil.returnConnction(connection)
        })
      }
    })

    ssc.start()
    ssc.awaitTermination()
  }

}
